# Universal Cmake build readme

## Notes

- Need to include vector and string, fstream.
- Make sure in TextureLoader you are loading JPG then RBG is set while creating texture.
- If you have STB included in ModelLoader and TExtureLoader then there might be issues of cyclic redudancy.
- Load textures/ shaders from ./../Resources


## Works for Win/ Mac and Ubuntu for now

Even though both mac and linux generate a .a file one cannot be used instead of the other. Libraries for mac and linux has to be generated individually

```bash
cd build
cmake ..

## for Mac and Ubuntu
make

## For Win64 VS project is created. Build the VS project.

```

Binary is created in the bin directory
then, in the bin directory to run the application
on Mac and Ubuntu

```bash
./ssrCmakeOpenGl
```


## cmake file 

```cmake

cmake_minimum_required(VERSION 3.11)

project(OpenGLCMakeTexAndGLM)

set(CMAKE_CXX_STANDARD 17)

# Create bin in build directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

find_package(OpenGL REQUIRED)
message(STATUS "OpenGl included at ${OPENGL_INCLUDE_DIR}")

# Add source files
file(GLOB_RECURSE SOURCE_FILES
	${CMAKE_SOURCE_DIR}/src/*.c
	${CMAKE_SOURCE_DIR}/src/*.cpp)

# Add header files
file(GLOB_RECURSE  HEADER_FILES 
	${CMAKE_SOURCE_DIR}/src/*.h 
	${CMAKE_SOURCE_DIR}/src/*.hpp)
  

add_executable(${PROJECT_NAME} ${HEADER_FILES} ${SOURCE_FILES})


set(GLM_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/dependencies/GLM")
set(STB_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/dependencies/STB")

list(APPEND INCLUDES 
      ${OPENGL_INCLUDE_DIRS}
      ${GLM_INCLUDE_DIR}
      ${STB_INCLUDE_DIR})


list(APPEND LIBRARIES ${OPENGL_LIBRARIES})


target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLFW/include ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLEW/include ${INCLUDES} )		
      
      
IF(UNIX AND NOT APPLE)
	set(LINUX TRUE)
ENDIF() 
  
IF(WIN32)

	## ONLY CREATE WIN64
	target_link_libraries(${PROJECT_NAME} PUBLIC  ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLFW/lib/Win64/glfw3.lib  ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLEW/lib/Win64/Release/glew32.lib ${LIBRARIES} )

      
ELSEIF(LINUX)

	find_package( Threads REQUIRED )

	list(APPEND LIBRARIES
	      ${CMAKE_THREAD_LIBS_INIT}
	      ${CMAKE_DL_LIBS})
			      
			      
	target_link_libraries(${PROJECT_NAME} PUBLIC  ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLFW/lib/ubuntu/libglfw3.a  ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLEW/lib/ubuntu/libGLEW.a ${LIBRARIES} )

ELSEIF(APPLE)

	find_library(COCOA_FRAMEWORK Cocoa)
	find_library(IOKIT_FRAMEWORK IOKit)
	find_library(CORE_FOUNDATION_FRAMEWORK CoreFoundation)
	find_library(CORE_VIDEO_FRAMEWORK CoreVideo)

	set(CMAKE_CXX_FLAGS "-framework Cocoa -framework OpenGL -framework IOKit")

	list(APPEND LIBRARIES 	     	
		${COCOA_FRAMEWORK}
		${OPENGL_gl_LIBRARY}
		${IOKIT_FRAMEWORK}
		${CORE_FOUNDATION_FRAMEWORK}
		${CORE_VIDEO_FRAMEWORK})

	target_link_libraries(${PROJECT_NAME} PUBLIC  ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLFW/lib/mac/libglfw3.a  ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GLEW/lib/mac/libGLEW.a ${LIBRARIES} )

ENDIF(WIN32)


add_custom_command(
	TARGET ${PROJECT_NAME} POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy_directory
	${PROJECT_SOURCE_DIR}/Assets
	$<TARGET_FILE_DIR:${PROJECT_NAME}>/Assets
)


if(WIN32)
    add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory
        ${PROJECT_SOURCE_DIR}/dll
        $<TARGET_FILE_DIR:${PROJECT_NAME}>)
endif()


```


## Output

![Output](zImages/ssr.png)
