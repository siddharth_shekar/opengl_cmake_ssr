#pragma once

//#include "Dependencies\glew\glew.h"
//#include "Dependencies\freeglut\freeglut.h"

#include <GL/glew.h>
#include <cstdio>


class GBuffer
{
public:
	GBuffer();
	~GBuffer();

	unsigned int gBuffer;
	unsigned int gPositionTex, gNormalTex, gAlbedoSpecTex, gDepthTex;

	void createBuffer();
	void bindBuffer();
	void unBindBuffer();
};

