#pragma once

//#include "Dependencies\glew\glew.h"
//#include "Dependencies\freeglut\freeglut.h"

#include <GL/glew.h>

#include "Utils.h"
#include "Camera.h"

#include "GameModel.h"
//#include "SimpleComputeParticle.h"
//#include "AdvancedComputeParticle.h"

//#include "Controls.h"

#include "Light.h"

#include "GBuffer.h"
#include "LightingFBO.h"


class Game {
	
	Light* light;

	GameModel* ssQuadPos;
	GameModel* ssQuadNor;
	GameModel* ssQuadCol;
	GameModel* ssQuadDep;
	GameModel* ssQuadLighting;

	GameModel* lighitngFullScreenQuad;
	GameModel* SSRFullScreenQuad;


	GameModel* floor;
	GameModel* cube;
	GameModel* bigCube;
	GameModel* sphere;
	GameModel* vQuad;
	GameModel* tri;



	LightingFBO* lightingFBO;
	GBuffer* gBuffer;

	
	// Controls* controls;

	 void rtQuadSetup();

public:

	Game();	 

	Camera* camera;

	//void updateKeyboardControls(Camera *camera);

	void render();

	void update(float dt);
};