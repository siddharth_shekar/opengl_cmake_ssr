#include "LightingFBO.h"
//#include "utils.h"
#include <cstdio>


LightingFBO::LightingFBO(){}


LightingFBO::~LightingFBO(){}

void LightingFBO::createBuffer(){

	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	// position color buffer
	glGenTextures(1, &lightingRT);
	glBindTexture(GL_TEXTURE_2D, lightingRT);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1280, 720, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lightingRT, 0);


	// //create and attach depth buffer (renderbuffer)
	//unsigned int rboDepth;
	//glGenRenderbuffers(1, &rboDepth);
	//glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
	//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1280, 720);
	//glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);

// finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		printf("++++++++ Framebuffer not complete! ++++++++++++++ \n");


	glBindFramebuffer(GL_FRAMEBUFFER, 0);


}

void LightingFBO::bindBuffer(){

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glClear(GL_COLOR_BUFFER_BIT );
	//glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

void LightingFBO::unBindBuffer(){

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
