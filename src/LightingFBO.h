#pragma once

//#include "Dependencies\glew\glew.h"
//#include "Dependencies\freeglut\freeglut.h"

#include <GL/glew.h>

class LightingFBO
{
public:
	LightingFBO();
	~LightingFBO();

	unsigned int fbo;
	unsigned int lightingRT;

	void createBuffer();
	void bindBuffer();
	void unBindBuffer();
};

