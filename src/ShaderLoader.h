#pragma once

//#include "Dependencies/glew/glew.h"
//#include "Dependencies/freeglut/freeglut.h"

#include <GL/glew.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>


class ShaderLoader
{
	private:

		 std::string ReadShader(const char *filename);
		 GLuint CreateShader(GLenum shaderType, std::string source, const char*  shaderName);

	public:

		ShaderLoader(void);
		~ShaderLoader(void);

		 GLuint CreateProgram(const char*  VertexShaderFilename, const char*  FragmentShaderFilename);
		 GLuint CreateProgram(const char* vertexShaderFilename, const char* fragmentShaderFilename, const char* geometryShaderFilename);
		 GLuint CreateProgram(char* vertexShaderFilename, char* fragmentShaderFilename, char* TessControlShaderFilename, char* TessEvalShaderFilename);

		 GLuint CreateProgram(char* ComputeShaderFilename);

};
