//http://in2gpu.com/2014/12/19/create-triangle-opengl-part-iii-attributes/
//http://in2gpu.com/opengl-3/
//http://www.tomdalling.com/blog/modern-opengl/05-model-assets-and-instances/
//http://stackoverflow.com/questions/5668388/drawing-an-array-of-objects-in-open-gl-glut
//http://www.badprog.com/c-opengl-creating-an-equilateral-triangle
//http://in2gpu.com/2014/12/02/create-triangle-opengl-part/
//billbaord/particles - http://www.opengl-tutorial.org/intermediate-tutorials/billboards-particles/particles-instancing/
//heightmap - http://nehe.gamedev.net/tutorial/beautiful_landscapes_by_means_of_height_mapping/16006/
//height map - http://www.lighthouse3d.com/opengl/terrain/index.php?heightmap
//tesselation = http://www.informit.com/articles/article.aspx?p=2120983
//source - http://github.prideout.net/
//quad tesselation - http://prideout.net/blog/?p=49
//mirroring - https://open.gl/depthstencils
//shader effects - http://www.geeks3d.com/shader-library/
//raom - http://www.gamasutra.com/view/feature/131596/realtime_dynamic_level_of_detail_.php


//#include "Utils.h"

//#include "Dependencies\glew\glew.h"
//#include "Dependencies\freeglut\freeglut.h"
//#include "Dependencies\soil\SOIL.h"

//#include "glm/glm.hpp"
//#include "glm/gtc/matrix_transform.hpp"
//#include "glm/gtc/type_ptr.hpp"


// GLEW needs to be included first
#include <GL/glew.h>

// GLFW is included next
#include <GLFW/glfw3.h>

#include <iostream>
#include <vector>

//#include"GLUTInterface.h"


#include "Game.h"
//#include "Controls.h"

Game* game = NULL;

float previousTime, currentTime = 0.0f;


GLfloat yaw = 0.0f;
GLfloat pitch = 0.0f;
GLfloat lastX = - Utils::WIDTH/2;
GLfloat lastY = Utils::HEIGHT/2;
bool firstMouse = false;

void update();
void render();
void updateKeyboard(GLFWwindow* window);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void updateMouse(GLFWwindow* window, double xpos, double ypos);

void setOpenGLSettings() {

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);//GL_FILL
	glEnable(GL_DEPTH_TEST); //enable the depth testing
	glDepthFunc(GL_LESS);

	//enable multisampling
	//glutSetOption(GLUT_MULTISAMPLE, 8);
	glEnable(GL_MULTISAMPLE);
	//glHint(GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST);

	//enable culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);// default
		
}


int main(int argc, char** argv) {

	//glutInit(&argc, argv);
	//glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA| GLUT_MULTISAMPLE);
	//glutInitWindowPosition(100, 40);
	//glutInitWindowSize(1280, 720);
	//glutCreateWindow("BASIC GAME");

	//init GLEW
	//GLint GlewInitResult = glewInit();

	//if (GLEW_OK != GlewInitResult){
	//	printf("ERROR: %s\n", glewGetErrorString(GlewInitResult));
	//	exit(EXIT_FAILURE);
	//}

	// GLFW

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);


#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	GLFWwindow* window = glfwCreateWindow(Utils::WIDTH, Utils::HEIGHT, " Hello OpenGL ", NULL, NULL);
	glfwMakeContextCurrent(window);

	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetCursorPos(window, Utils::WIDTH / 2.0f, Utils::HEIGHT / 2.0f);



	//init GLEW
	glewExperimental = GL_TRUE;
	glewInit();


	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* vendor = glGetString(GL_VENDOR);
	const GLubyte* version = glGetString(GL_VERSION);
	const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
	GLint major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	printf("GL Vendor : %s\n", vendor);
	printf("GL Renderer : %s\n", renderer);
	printf("GL Version (string) : %s\n", version);
	printf("GL Version (integer) : %d.%d\n", major, minor);
	printf("GLSL Version : %s\n", glslVersion);


	setOpenGLSettings();

	//controls = new Controls();
	game = new Game();


	while (!glfwWindowShouldClose(window)) {

		glfwPollEvents();
		updateKeyboard(window);

		update();

		render();

		glfwSwapBuffers(window);

		previousTime = currentTime;

	}

	glfwTerminate();



	//GLUTInterface::getInstance();

	//glutIdleFunc(update);
	//glutDisplayFunc(render);

	//glutKeyboardFunc(GLUTInterface::getInstance()->keyboard);
	//glutKeyboardUpFunc(GLUTInterface::getInstance()->keyboard_up);
	//glutMouseFunc(GLUTInterface::getInstance()->mouse);
	//glutPassiveMotionFunc(GLUTInterface::getInstance()->mousePassiveMove);

	//glutMainLoop();
	return 0;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {

	updateMouse(window, xpos, ypos);

}


void update() {

	//float currentTime = glutGet(GLUT_ELAPSED_TIME);
	//currentTime = currentTime / 1000.0f;

	currentTime = glfwGetTime();
	float dt = previousTime - currentTime;

	//GLUTInterface::getInstance()->update(currentTime);

	game->update(dt);

	previousTime = currentTime;

	//glutPostRedisplay();
}

void render() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glClearColor(0.2f, 0.2f, 1.0f, 0.0f);

	//GLUTInterface::getInstance()->render();

	game->render();
	
	//glutSwapBuffers();
}

void updateKeyboard(GLFWwindow *window){

	
		 if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);
	
		 if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			game->camera->moveForward();
			
		 if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			 game->camera->moveBack();
		
		 if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			 game->camera->moveLeft();
		
		 if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			 game->camera->moveRight();

		 if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			 game->camera->moveUp();

		 if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			 game->camera->moveDown();
	

}

void updateMouse(GLFWwindow *window, double xpos, double ypos){

	
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;


	float sensitivity = 0.5;	// Change this value to your liking
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	// Make sure that when pitch is out of bounds, screen doesn't get flipped
	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	
	game->camera->setCameraFront(glm::normalize(front));
	

}

