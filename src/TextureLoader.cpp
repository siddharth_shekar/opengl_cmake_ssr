#include "TextureLoader.h"


#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>


TextureLoader::TextureLoader(){
}


GLuint TextureLoader::getTextureID(const char* texFileName){

	//** loadImage and create texture
	int width, height, channels;

	//unsigned char* image = SOIL_load_image(texFileName.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);	
	
	stbi_uc* image = stbi_load(texFileName, &width, &height, &channels, STBI_rgb);

	if (image == NULL) {
	
		assert("image is null!!");
	}


	printf("TextureLoader: fileName %s \n", texFileName);
	printf("Channel Count:  %d \n", channels);

	GLuint mtexture;

	//** load texture
	glGenTextures(1, &mtexture);
	glBindTexture(GL_TEXTURE_2D, mtexture);

	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	

	//// Load image, create texture and generate mipmaps

	if(channels == 4)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	else if(channels ==3)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	//
	//glGenerateMipmap(GL_TEXTURE_2D);
	
	//SOIL_free_image_data(image);



	return mtexture;
}

TextureLoader::~TextureLoader()
{
}
