#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

//#include "Dependencies\glew\glew.h"
//#include "Dependencies\freeglut\freeglut.h"
//#include "Dependencies\soil\SOIL.h"


#include <GL/glew.h>

class TextureLoader
{
public:
	TextureLoader();
	~TextureLoader();

	GLuint getTextureID(const char* texFileName);
};

