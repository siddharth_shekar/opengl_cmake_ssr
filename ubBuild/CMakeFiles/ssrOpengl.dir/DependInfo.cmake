# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/Camera.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/Camera.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/Controls.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/Controls.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/GBuffer.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/GBuffer.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/Game.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/Game.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/GameModel.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/GameModel.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/Light.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/Light.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/LightingFBO.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/LightingFBO.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/ShaderLoader.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/ShaderLoader.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/Source.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/Source.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/TextureLoader.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/TextureLoader.cpp.o"
  "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/src/Utils.cpp" "/home/sidubuntu/Desktop/myRepos/ssrCmakeOpengl/ubBuild/CMakeFiles/ssrOpengl.dir/src/Utils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../dependencies/GLFW/include"
  "../dependencies/GLEW/include"
  "../dependencies/GLM"
  "../dependencies/STB"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
