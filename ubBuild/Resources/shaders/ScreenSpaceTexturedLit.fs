#version 410 core

out vec4 fragColor;

in vec2 TexCoord;


uniform vec3 cameraPos;
uniform vec3 lightPos;
uniform vec3 lightColor;

uniform mat4 invViewMatrix;


uniform float ambientStrength;

uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;
uniform sampler2D gDepth;

void main(){
		
		vec2 tCoord = vec2 (TexCoord.x, 1 - TexCoord.y);
		vec3 Diffuse = texture(gAlbedoSpec, tCoord).rgb;
		float spec = texture(gAlbedoSpec, tCoord).a;

		// SSR
		
		vec3 viewNormal = texture(gNormal, tCoord).rgb;
		vec3 viewPos = textureLod(gPosition, tCoord, 2).xyz;
		vec3 albedo = texture(gAlbedoSpec, tCoord).rgb;
		
		
		vec4 WorldPos4 = invViewMatrix * vec4 (viewPos, 1.0) ;
		vec4 Normal4 = invViewMatrix * vec4(viewNormal , 0.0);
		
		vec3 WorldPos = WorldPos4.xyz;
		vec3 Normal = Normal4.xyz; 
		
			
		// Lighting
		
		
		//**ambient
		vec3 ambient = ambientStrength * Diffuse.rgb;
		
		//**diffuse
		vec3 lightDir = normalize(lightPos - WorldPos);
		float diff = max(dot(Normal, lightDir), 0.0);
		vec3 diffuse = diff * lightColor;
		
		//**specular 
		vec3 viewDir = normalize(cameraPos - WorldPos);
		
		//** blinn Phong Model
		vec3 H = normalize( lightDir + viewDir);
		vec3 specular = pow(max(dot(Normal, H), 0.0), 128) * lightColor * spec;

		//** rim lighting
		float f = 1.0 - dot(Normal, viewDir);// Calculate the rim factor 
		f = smoothstep(0.0, 1.0, f);// Constrain it to the range 0 to 1 using a smoothstep function     
		f = pow(f, 16);// Raise it to the rim exponent 
		vec3 rim =  f * vec3(1.0f, 0.0f, 0.0f) * lightColor;// Finally, multiply it by the rim color

		// old lighting calculation
		vec3 totalColor = (ambient + diffuse + specular) * Diffuse.rgb; // * objectColor;

		//float shadow = ShadowCalculation(FragPosLightSpace);
		// new lighting calculation with shadow
		//vec3 totalColor = ambient + ((shadow) * (diffuse + specular + rim));
		
		vec3 lighting = totalColor;
		
		
		fragColor = vec4(lighting, 1.0f);
	
    }
		

		
