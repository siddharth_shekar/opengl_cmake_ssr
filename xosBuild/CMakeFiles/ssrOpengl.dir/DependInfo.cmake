# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/Camera.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/Camera.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/GBuffer.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/GBuffer.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/Game.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/Game.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/GameModel.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/GameModel.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/Light.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/Light.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/LightingFBO.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/LightingFBO.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/ShaderLoader.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/ShaderLoader.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/Source.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/Source.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/TextureLoader.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/TextureLoader.cpp.o"
  "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/src/Utils.cpp" "/Users/siddharthshekar/Desktop/myRepos/ssrcmakeopengl/xosBuild/CMakeFiles/ssrOpengl.dir/src/Utils.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../dependencies/GLFW/include"
  "../dependencies/GLEW/include"
  "../dependencies/GLM"
  "../dependencies/STB"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
